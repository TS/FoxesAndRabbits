##Script, executed as in documentation
#(Results will be different due to stochastic data generation)

using .FoxesAndRabbits

#make population and fit hybrid model
population = make_population_data([1.0, 1.0], 8.0, stochastic = true)
pred_pop = learn!(population)

#make test set, continuing in time
initial_rabbits = population.rabbits[end]
initial_foxes = population.foxes[end]
t_end = 5.0
new_population = make_population_data([initial_rabbits, initial_foxes], t_end, stochastic = true)

#predict and make composed data struct
predicted_rabbits = predict(pred_pop.parameters,new_population)
modelled_data = FoxesAndRabbits.PopulationData(new_population.time,new_population.foxes,predicted_rabbits)
test_population = FoxesAndRabbits.PredictedPopulationData(new_population,modelled_data,pred_pop.parameters)

#run ensemble
uncertainty = 0.3
trajectories = 50
ensemble_sol = predict_ensemble(new_population,pred_pop.parameters, uncertainty, trajectories = trajectories)

#plot everything

visualize(population)
visualize(pred_pop)
visualize(test_population)
visualize(ensemble_sol)